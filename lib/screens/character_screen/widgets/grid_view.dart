import 'package:flutter/material.dart';
import './status_grid_view.dart';
import '../../../models/person.dart';

class GridViewCharacter extends StatelessWidget {
  const GridViewCharacter({
    Key? key,
    required this.personsList,
  }) : super(key: key);

  final List<Person> personsList;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      mainAxisSpacing: 20,
      crossAxisSpacing: 8,
      childAspectRatio: 0.78,
      crossAxisCount: 2,
      padding: const EdgeInsets.only(
        top: 12,
        left: 12,
        right: 12,
      ),
      children: personsList.map(
        (person) {
          return InkWell(
            child: StatusGridView(person),
            onTap: () {},
          );
        },
      ).toList(),
    );
  }
}
