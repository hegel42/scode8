import 'package:flutter/material.dart';

import '../../../models/person.dart';

import './status_list_view.dart';

class ListViewCharacter extends StatelessWidget {
  const ListViewCharacter({
    Key? key,
    required this.personsList,
  }) : super(key: key);

  final List<Person> personsList;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: personsList.length,
      separatorBuilder: (context, _) => const SizedBox(height: 20),
      itemBuilder: (context, index) {
        return InkWell(
          child: StatusListView(personsList[index]),
          onTap: () {},
        );
      },
    );
  }
}
