import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simplecode_3/bloc/persons/bloc_persons.dart';

import '../../bloc/persons/states.dart';
import '../../constants/app_colors.dart';
import '../../generated/l10n.dart';
import './widgets/search_bar.dart';
import '../../widgets/bottom_nav_bar.dart';
import '../../constants/app_fonts.dart';

import './widgets/list_view.dart';
import './widgets/grid_view.dart';

class PersonScreen extends StatelessWidget {
  const PersonScreen({Key? key}) : super(key: key);

  static final isListView = ValueNotifier(true);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const BottomNavBar(
          selectedIndex: 0,
        ),
        backgroundColor: AppColors.designWhite,
        body: Column(
          children: [
            SearchBar(
              onChanged: (value) {
                BlocProvider.of<BlocPersons>(context).add(
                  EventPersonsFilterByName(value),
                );
              },
            ),
            BlocBuilder<BlocPersons, StateBlocPersons>(
              builder: (context, state) {
                var personsTotal = 0;
                if (state is StatePersonsData) {
                  personsTotal = state.data.length;
                }

                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          '${S.of(context).totalCharacters.toUpperCase()} : $personsTotal',
                          style: AppTextStyle.s12w500.copyWith(
                            color: AppColors.designGrey,
                          ),
                        ),
                      ),
                      isListView.value
                          ? IconButton(
                              icon: const Icon(
                                Icons.list,
                                color: AppColors.designGrey,
                                size: 28,
                              ),
                              onPressed: () {
                                isListView.value = !isListView.value;
                              },
                            )
                          : IconButton(
                              icon: const Icon(
                                Icons.grid_view,
                                color: AppColors.designGrey,
                                size: 28,
                              ),
                              onPressed: () {
                                isListView.value = !isListView.value;
                              },
                            ),
                    ],
                  ),
                );
              },
            ),
            Expanded(
              child: BlocBuilder<BlocPersons, StateBlocPersons>(
                builder: (context, state) {
                  return state.when(
                    initial: () => const SizedBox.shrink(),
                    loading: () {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          CircularProgressIndicator(),
                        ],
                      );
                    },
                    data: (data) {
                      if (data.isEmpty) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(S.of(context).characterListIsEmpty),
                            ),
                          ],
                        );
                      } else {
                        return ValueListenableBuilder<bool>(
                          valueListenable: isListView,
                          builder: (context, isListViewMode, _) {
                            return isListViewMode
                                ? ListViewCharacter(personsList: data)
                                : GridViewCharacter(
                                    personsList: data,
                                  );
                          },
                        );
                      }
                    },
                    error: (error) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(error),
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
