import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:simplecode_3/constants/app_fonts.dart';
import '../../../models/location.dart';

import '../../../constants/app_colors.dart';
import '../../../generated/l10n.dart';

class LocationPage extends StatelessWidget {
  const LocationPage({
    Key? key,
    required this.location,
  }) : super(key: key);

  final Location location;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.designWhite,
        appBar: AppBar(
          title: Text(
            location.name.toString(),
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    location.name.toString(),
                    style: AppTextStyle.s24w500,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      location.type.toString(),
                      style: AppTextStyle.secTextStyle,
                    ),
                    Text(
                      location.dimension.toString(),
                      style: AppTextStyle.secTextStyle,
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                      '${S.of(context).aired}: ${DateFormat.yMMMMEEEEd().format(location.created as DateTime)} '),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
