// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simplecode_3/bloc/locations/bloc_locations.dart';
import 'package:simplecode_3/bloc/locations/states.dart';
import 'package:simplecode_3/constants/app_colors.dart';
import 'package:simplecode_3/generated/l10n.dart';
import 'package:simplecode_3/screens/locations_list/widgets/listview.dart';
import 'package:simplecode_3/screens/locations_list/widgets/search_bar.dart';
import 'package:simplecode_3/widgets/bottom_nav_bar.dart';

import '../../constants/app_fonts.dart';
import 'widgets/location_alert_dialog.dart';

class LocationsList extends StatelessWidget {
  const LocationsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const BottomNavBar(selectedIndex: 1),
        backgroundColor: AppColors.designWhite,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // LocationsSearchBar(
            //   // searchController: ,
            //   onChanged: (value) {
            //     BlocProvider.of<BlocLocations>(context).add(
            //       EventLocationsFilterByName(value),
            //     );
            //   },
            // ),

            BlocBuilder<BlocLocations, StateBlocLocations>(
              builder: (context, state) {
                var searchText = '';
                if (state is StateLocationsData) {
                  if (state.searchText != null) {
                    searchText = state.searchText as String;
                    // didn't work!
                  }
                }
                return LocationsSearchBar(
                  onChanged: (value) {
                    BlocProvider.of<BlocLocations>(context).add(
                      EventLocationsFilterByName(value),
                    );
                  },
                );
              },
            ),

            BlocBuilder<BlocLocations, StateBlocLocations>(
              builder: (context, state) {
                var locationTotal = 0;
                if (state is StateLocationsData) {
                  locationTotal = state.data.length;
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Text(
                    '${S.of(context).totalLocations.toUpperCase()}: $locationTotal',
                    style: AppTextStyle.s12w500.copyWith(
                      color: AppColors.designGrey,
                    ),
                  ),
                );
              },
            ),
            Expanded(
              child: BlocBuilder<BlocLocations, StateBlocLocations>(
                buildWhen: (previous, current) {
                  if (previous.runtimeType != current.runtimeType) {
                    return true;
                  } else {
                    return previous.mapOrNull(
                            data: (state) => state.data.length) !=
                        current.mapOrNull(data: (state) => state.data.length);
                  }
                },
                builder: (context, state) {
                  return state.map(
                    initial: (_) => const SizedBox.shrink(),
                    loading: (_) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                    data: (state) {
                      if (state.data.isEmpty) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(
                                S.of(context).noLocation,
                                style: AppTextStyle.s14w400,
                              ),
                            ),
                          ],
                        );
                      } else {
                        return LocationsListView(
                          locationsList: state.data,
                        );
                      }
                    },
                    error: (state) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(state.error),
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            ),
            BlocConsumer<BlocLocations, StateBlocLocations>(
              builder: (context, state) {
                final isLoading = state.maybeMap(
                  data: (state) => state.isLoading,
                  orElse: () => false,
                );
                return Center(
                    child: isLoading
                        ? const LinearProgressIndicator()
                        : const SizedBox.shrink());
              },
              listener: (context, state) {
                state.mapOrNull(
                  data: (state) {
                    if (state.errorMessage != null) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return LocationAlertDialog(
                            onError: state.errorMessage!,
                          );
                        },
                      );
                    }
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
