import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simplecode_3/bloc/locations/bloc_locations.dart';
import 'package:simplecode_3/constants/app_colors.dart';
import 'package:simplecode_3/screens/locations_list/location_page/location_page.dart';
import 'package:simplecode_3/screens/locations_list/widgets/listview_tile.dart';
import '../../../models/location.dart';

import '../../../generated/l10n.dart';

class LocationsListView extends StatelessWidget {
  const LocationsListView({Key? key, required this.locationsList})
      : super(key: key);

  final List<Location> locationsList;

  @override
  Widget build(BuildContext context) {
    if (locationsList.isEmpty) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Text(S.of(context).noLocation),
          ),
        ],
      );
    } else {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<BlocLocations>(context).add(
            EventLocationsFetch(),
          );
        },
        child: NotificationListener(
          onNotification: (notification) {
            if (notification is ScrollNotification) {
              if (notification.metrics.extentAfter == 0) {
                BlocProvider.of<BlocLocations>(context).add(
                  EventLocationsNextPage(),
                );
              }
            }
            return false;
          },
          child: ListView.separated(
            itemBuilder: (context, index) {
              return InkWell(
                child: LocationListviewTile(locationsList[index]),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LocationPage(
                        location: locationsList[index],
                      ),
                    ),
                  );
                },
              );
            },
            separatorBuilder: (context, _) => const Divider(
              color: AppColors.mainText,
              endIndent: 20,
              indent: 20,
            ),
            itemCount: locationsList.length,
          ),
        ),
      );
    }
  }
}
