import 'package:flutter/material.dart';

import '../../../constants/app_fonts.dart';
import '../../../generated/l10n.dart';

class LocationAlertDialog extends StatelessWidget {
  const LocationAlertDialog({
    Key? key,
    required this.onError,
  }) : super(key: key);

  final String onError;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        S.of(context).error,
        style: AppTextStyle.mainTextStyle,
      ),
      content: Text(
        onError,
        style: AppTextStyle.secTextStyle,
      ),
    );
  }
}
