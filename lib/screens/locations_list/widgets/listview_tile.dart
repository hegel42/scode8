import 'package:flutter/material.dart';

import 'package:simplecode_3/constants/app_colors.dart';
import 'package:simplecode_3/constants/app_fonts.dart';
import '../../../models/location.dart';

class LocationListviewTile extends StatelessWidget {
  const LocationListviewTile(
    this.location, {
    Key? key,
  }) : super(key: key);

  final Location location;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      location.type as String,
                      style: AppTextStyle.s16w400
                          .copyWith(color: AppColors.designGrey),
                    ),
                    Text(
                      location.name as String,
                      style: AppTextStyle.mainTextStyle,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      location.dimension as String,
                      style: AppTextStyle.secTextStyle,
                    ),
                  ],
                ),
              ),
              const Icon(Icons.arrow_forward)
            ],
          ),
        ],
      ),
    );
  }
}
