// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'dart:core';

import '../../../constants/app_colors.dart';
import '../../../constants/app_fonts.dart';
import '../../../models/episode.dart';

class EpisodesTile extends StatelessWidget {
  const EpisodesTile(
    this.episode, {
    Key? key,
  }) : super(key: key);

  final Episode episode;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            AppColors.designGrey,
            AppColors.primary,
          ],
        ),
        borderRadius: BorderRadius.circular(6),
        color: AppColors.background1,
        boxShadow: const [
          BoxShadow(
            color: AppColors.designGrey,
            offset: Offset(0, 1),
            blurRadius: 3,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  episode.episode as String,
                  style: AppTextStyle.s14w400.copyWith(
                    color: AppColors.designWhite,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  episode.name as String,
                  style: AppTextStyle.mainTextStyle.copyWith(
                    color: AppColors.designWhite,
                  ),
                  overflow: TextOverflow.fade,
                  softWrap: false,
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  episode.air_date as String,
                  style: AppTextStyle.mainTextStyle.copyWith(
                    color: AppColors.designWhite,
                  ),
                ),
              ],
            ),
          ),
          const Icon(
            Icons.arrow_forward_ios_sharp,
            color: AppColors.designWhite,
          ),
        ],
      ),
    );
  }
}
