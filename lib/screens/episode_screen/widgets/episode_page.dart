import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:simplecode_3/constants/app_colors.dart';
import 'package:simplecode_3/constants/app_fonts.dart';

import '../../../generated/l10n.dart';
import '../../../models/episode.dart';

class EpisodePage extends StatelessWidget {
  const EpisodePage({Key? key, required this.episode}) : super(key: key);

  final Episode episode;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.designWhite,
        appBar: AppBar(
          backgroundColor: AppColors.designWhite,
          elevation: 0,
          iconTheme: const IconThemeData(
            color: Colors.black,
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  episode.name.toString(),
                  style: AppTextStyle.s24w500,
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 10,
                  bottom: 20,
                ),
                child: Text(
                  episode.episode.toString(),
                  style: AppTextStyle.secTextStyle,
                ),
              ),
              Text(
                '${S.of(context).aired}:  ${DateFormat.yMMMMEEEEd().format(DateTime.parse(episode.created!))}',
                style: AppTextStyle.s14w400,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
