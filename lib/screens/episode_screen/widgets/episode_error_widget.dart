import 'package:flutter/material.dart';

class EpisodeErrorWidget extends StatelessWidget {
  const EpisodeErrorWidget({Key? key, required this.onError}) : super(key: key);

  final String onError;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          child: Text(
            onError,
          ),
        )
      ],
    );
  }
}
