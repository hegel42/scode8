import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simplecode_3/bloc/episodes/bloc_episodes.dart';
import 'package:simplecode_3/bloc/episodes/states.dart';
import 'package:simplecode_3/constants/app_fonts.dart';
import 'package:simplecode_3/screens/episode_screen/widgets/body.dart';
import 'package:simplecode_3/screens/episode_screen/widgets/episode_error_widget.dart';
import 'package:simplecode_3/widgets/bottom_nav_bar.dart';

import '../../constants/app_colors.dart';
import '../../generated/l10n.dart';

class EpisodeScreen extends StatelessWidget {
  const EpisodeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavBar(selectedIndex: 2),
      appBar: AppBar(
        backgroundColor: AppColors.designWhite,
        centerTitle: true,
        elevation: 0,
        title: Text(
          S.of(context).episodes,
          style: AppTextStyle.s24w500,
        ),
      ),
      backgroundColor: AppColors.designWhite,
      body: Stack(
        children: [
          Positioned.fill(
            child: Column(
              children: [
                Expanded(
                  child: BlocBuilder<BlocEpisodes, StateBlocEpisodes>(
                    buildWhen: (previous, current) {
                      if (previous.runtimeType != current.runtimeType) {
                        return true;
                      } else {
                        return previous.mapOrNull(
                                data: (state) => state.data.length) !=
                            current.mapOrNull(
                                data: (state) => state.data.length);
                      }
                    },
                    builder: (context, state) {
                      return state.map(
                        initial: (_) => const SizedBox.shrink(),
                        loading: (_) => const Center(
                          child: CircularProgressIndicator(),
                        ),
                        data: (state) => Body(data: state.data),
                        error: (state) =>
                            EpisodeErrorWidget(onError: state.error),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          BlocConsumer<BlocEpisodes, StateBlocEpisodes>(
            builder: (context, state) {
              final isLoading = state.maybeMap(
                data: (state) => state.isLoading,
                orElse: () => false,
              );
              return Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: isLoading
                    ? const LinearProgressIndicator()
                    : const SizedBox.shrink(),
              );
            },
            listener: (context, state) {
              state.mapOrNull(
                data: (state) {
                  if (state.errorMessage != null) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text(
                            S.of(context).error,
                            style: AppTextStyle.mainTextStyle,
                          ),
                          content: Text(
                            state.errorMessage!,
                            style: AppTextStyle.secTextStyle,
                          ),
                        );
                      },
                    );
                  }
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
