// ignore_for_file: avoid_print

import 'package:simplecode_3/repo/api.dart';

import '../models/location.dart';

import 'package:simplecode_3/generated/l10n.dart';

class RepoLocations {
  RepoLocations({required this.api});

  final Api api;

  Future<ResultRepoLocations> filterByLocation(String name) async {
    try {
      final result = await api.dio.get(
        '/location',
        queryParameters: {"name": name},
      );
      final List locationsListJson = result.data['results'] ?? [];
      final locationsList = locationsListJson
          .map(
            (item) => Location.fromJson(item),
          )
          .toList();

      return ResultRepoLocations(
        locationsList: locationsList,
        searchText: name,
      );
    } catch (error) {
      print('Error: $error');
      return ResultRepoLocations(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }

  Future<ResultRepoLocations> fetch() => nextPage(1);
  Future<ResultRepoLocations> nextPage(int page) async {
    try {
      final result = await api.dio.get('/location?page=$page');
      final isEndOfData = result.data['info']['next'] == null;
      final List json = result.data['results'] ?? [];
      final list = json.map((item) => Location.fromJson(item));
      return ResultRepoLocations(
        locationsList: list.toList(),
        isEndOfData: isEndOfData,
      );
    } catch (error) {
      print('Error: $error');
      return ResultRepoLocations(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultRepoLocations {
  ResultRepoLocations({
    this.errorMessage,
    this.locationsList,
    this.isEndOfData,
    this.searchText,
  });

  final String? errorMessage;
  final List<Location>? locationsList;
  final bool? isEndOfData;
  final String? searchText;
}
