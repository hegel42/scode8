// ignore_for_file: avoid_print

import '../generated/l10n.dart';
import '../models/episode.dart';
import 'api.dart';

class RepoEpisodes {
  final Api api;

  RepoEpisodes({required this.api});

  Future<ResultRepoEpisodes> fetch() => nextPage(1);
  Future<ResultRepoEpisodes> nextPage(int page) async {
    try {
      final result = await api.dio.get('/episode?page=$page');
      final isEndOfData = result.data['info']['next'] == null;
      final List json = result.data['results'] ?? [];
      final list = json.map((item) => Episode.fromJson(item));
      return ResultRepoEpisodes(
        episodes: list.toList(),
        isEndOfData: isEndOfData,
      );
    } catch (error) {
      print('Error: $error');
      return ResultRepoEpisodes(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultRepoEpisodes {
  ResultRepoEpisodes({
    this.errorMessage,
    this.episodes,
    this.isEndOfData,
  });

  final String? errorMessage;
  final List<Episode>? episodes;
  final bool? isEndOfData;
}
