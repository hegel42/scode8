part of 'bloc_locations.dart';

abstract class EventBlocLocations {}

class EventLocationsFilterByName extends EventBlocLocations {
  EventLocationsFilterByName(this.name);

  final String name;
}

class EventLocationsFetch extends EventBlocLocations {}

class EventLocationsNextPage extends EventBlocLocations {}
