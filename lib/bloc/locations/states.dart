import '../../models/location.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'states.freezed.dart';

@freezed
class StateBlocLocations with _$StateBlocLocations {
  const factory StateBlocLocations.initial() = StateLocationsInitial;
  const factory StateBlocLocations.loading() = StateLocationsLoading;
  const factory StateBlocLocations.data({
    required List<Location> data,
    @Default(false) bool isLoading,
    String? errorMessage,
    String? searchText,
  }) = StateLocationsData;
  const factory StateBlocLocations.error(String error) = StateLocationsError;
}
