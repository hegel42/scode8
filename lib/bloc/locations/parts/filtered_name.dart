part of '../bloc_locations.dart';

extension FilteredByName on BlocLocations {
  Future<void> _filter(
    EventLocationsFilterByName event,
    Emitter<StateBlocLocations> emit,
  ) async {
    emit(const StateBlocLocations.loading());
    final result = await repo.filterByLocation(event.name);
    if (result.errorMessage != null) {
      emit(
        StateBlocLocations.error(result.errorMessage!),
      );
      return;
    }
    emit(
      StateBlocLocations.data(
        data: result.locationsList!,
        searchText: result.searchText,
      ),
    );
  }
}
