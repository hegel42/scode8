part of '../bloc_locations.dart';

extension Fetch on BlocLocations {
  Future<void> _fetch(
    EventLocationsFetch event,
    Emitter<StateBlocLocations> emit,
  ) async {
    _isEndOfData = false;
    emit(const StateBlocLocations.loading());
    final result = await repo.fetch();
    if (result.errorMessage != null) {
      emit(StateBlocLocations.error(result.errorMessage!));
      return;
    }
    emit(
      StateBlocLocations.data(data: result.locationsList!),
    );
    _currentPage = 1;
  }
}
