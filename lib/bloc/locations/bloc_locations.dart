// ignore_for_file: prefer_final_fields

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/location.dart';
import 'package:simplecode_3/repo/repo_locations.dart';
import 'states.dart';
part 'location_events.dart';
part 'parts/fetch.dart';
part 'parts/next_page.dart';
part 'parts/filtered_name.dart';

class BlocLocations extends Bloc<EventBlocLocations, StateBlocLocations> {
  final RepoLocations repo;

  BlocLocations({required this.repo, this.searchText})
      : super(const StateBlocLocations.initial()) {
    on<EventLocationsFilterByName>(_filter);
    on<EventLocationsFetch>(_fetch);
    on<EventLocationsNextPage>(_nextPage);
  }
  String? searchText;
  int _currentPage = 1;
  bool _isEndOfData = false;
  bool _isInProgress = false;
}
