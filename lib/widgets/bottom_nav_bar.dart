import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:simplecode_3/constants/app_colors.dart';
import 'package:simplecode_3/screens/episode_screen/episode_screen.dart';
import 'package:simplecode_3/screens/locations_list/locations_list.dart';

import '../constants/app_assets.dart';
import '../generated/l10n.dart';
import '../screens/character_screen/person_screen.dart';
import '../screens/settings_screen.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({
    Key? key,
    required this.selectedIndex,
  }) : super(key: key);

  final int selectedIndex;

  PageRouteBuilder _pageRouteBuilder(Widget screen) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => screen,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        const curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
      transitionDuration: const Duration(
        microseconds: 500,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.characterIcon,
            color: selectedIndex == 0 ? null : AppColors.menuBarText,
          ),
          label: S.of(context).characters,
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.locationsIcon,
            color: selectedIndex == 1 ? AppColors.primary : null,
          ),
          label: S.of(context).locations,
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.episodesIcon,
            color: selectedIndex == 2 ? AppColors.primary : null,
          ),
          label: S.of(context).episodes,
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.settings,
            color: selectedIndex == 3 ? AppColors.primary : null,
          ),
          label: S.of(context).settings,
        ),
      ],
      selectedItemColor: AppColors.primary,
      unselectedItemColor: AppColors.menuBarText,
      currentIndex: selectedIndex,
      type: BottomNavigationBarType.fixed,
      // selectedFontSize: 15,
      // showUnselectedLabels: true,
      onTap: (index) {
        if (index == 0) {
          Navigator.of(context).pushAndRemoveUntil(
            _pageRouteBuilder(const PersonScreen()),
            (route) => false,
          );
        } else if (index == 2) {
          Navigator.of(context).pushAndRemoveUntil(
            _pageRouteBuilder(const EpisodeScreen()),
            (route) => false,
          );
        } else if (index == 1) {
          Navigator.of(context).pushAndRemoveUntil(
              _pageRouteBuilder(const LocationsList()), (route) => false);
        } else if (index == 3) {
          Navigator.of(context).pushAndRemoveUntil(
            _pageRouteBuilder(const SettingsScreen()),
            (route) => false,
          );
        }
      },
    );
  }
}
