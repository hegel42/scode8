import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simplecode_3/bloc/episodes/bloc_episodes.dart';
import 'package:simplecode_3/bloc/locations/bloc_locations.dart';
import 'package:simplecode_3/bloc/persons/bloc_persons.dart';
import 'package:simplecode_3/repo/api.dart';
import 'package:simplecode_3/repo/repo_characters.dart';
import 'package:simplecode_3/repo/repo_episodes.dart';
import 'package:simplecode_3/repo/repo_locations.dart';
import '../repo/repo_settings.dart';

class InitWidget extends StatelessWidget {
  const InitWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => Api(),
        ),
        RepositoryProvider(
          create: (context) => RepoSettings(),
        ),
        RepositoryProvider(
          create: (context) => RepoPersons(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
        RepositoryProvider(
          create: (context) => RepoLocations(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
        RepositoryProvider(
          create: (context) => RepoEpisodes(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
      ],
      child: MultiBlocProvider(
        child: child,
        providers: [
          BlocProvider(
            create: (context) => BlocPersons(
              repo: RepositoryProvider.of<RepoPersons>(context),
            )..add(
                EventPersonsFilterByName(''),
              ),
          ),
          BlocProvider(
            create: (context) => BlocLocations(
              repo: RepositoryProvider.of<RepoLocations>(context),
            )
              ..add(
                EventLocationsFilterByName(''),
              )
              ..add(
                EventLocationsFetch(),
              ),
          ),
          BlocProvider(
            create: (context) => BlocEpisodes(
              repo: RepositoryProvider.of<RepoEpisodes>(context),
            )..add(
                EventEpisodesFetch(),
              ),
          ),
        ],
      ),
    );
  }
}
